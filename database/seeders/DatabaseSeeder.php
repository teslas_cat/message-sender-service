<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserNotificationSettings;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         User::factory(5)
             ->has(UserNotificationSettings::factory(), 'notificationSettings')
             ->create();
    }
}
