<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserNotificationSettings>
 */
class UserNotificationSettingsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'phone_number' => $this->faker->e164PhoneNumber(),
            'slack_channel' => '#' . $this->faker->slug(4),
            'slack_token' => Str::random(),
            'webhook_url' => $this->faker->url(),
        ];
    }

    /**
     * Indicate that the model's phone should be empty.
     */
    public function emptyPhone(): static
    {
        return $this->state(fn (array $attributes) => [
            'phone_number' => null,
        ]);
    }

    /**
     * Indicate that the model's slack channel data should be empty.
     */
    public function emptySlackChannel(): static
    {
        return $this->state(fn (array $attributes) => [
            'slack_channel' => null,
            'slack_token' => null,
        ]);
    }

    /**
     * Indicate that the model's webhook url should be empty.
     */
    public function emptyWebhook(): static
    {
        return $this->state(fn (array $attributes) => [
            'webhook_url' => null,
        ]);
    }
}
