<?php

namespace App\Models;

use App\Enums\NotificationChannel;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Slack\SlackRoute;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property Carbon|null $email_verified_at
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 *
 * Relations
 * @property UserNotificationSettings $notificationSettings
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * Route notifications for the Vonage channel.
     */
    public function routeNotificationForVonage(Notification $notification): string
    {
        return $this->notificationSettings->phone_number;
    }

    /**
     * Route notifications for the Slack channel.
     */
    public function routeNotificationForSlack(Notification $notification): SlackRoute
    {
        $settings = $this->notificationSettings;

        return SlackRoute::make($settings->slack_channel, $settings->slack_token);
    }

    /**
     * Route notifications for the Webhook channel.
     */
    public function routeNotificationForWebhook(): string
    {
        return $this->notificationSettings->webhook_url;
    }

    /**
     * Checks if user has all necessary data to be notified via channel.
     * @param NotificationChannel $channel
     * @return bool
     */
    public function canBeNotifiedViaChannel(NotificationChannel $channel): bool
    {
        $settings = $this->notificationSettings;
        return match ($channel) {
            NotificationChannel::Email => true,
            NotificationChannel::SMS => !is_null($settings->phone_number),
            NotificationChannel::Slack => !is_null($settings->slack_channel) && !is_null($settings->slack_token),
            NotificationChannel::Webhook => !is_null($settings->webhook_url),
        };
    }

    /**
     * @return HasOne
     */
    public function notificationSettings(): HasOne
    {
        return $this->hasOne(UserNotificationSettings::class);
    }
}
