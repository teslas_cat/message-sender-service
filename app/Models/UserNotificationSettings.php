<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class User
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $phone_number
 * @property string $slack_channel
 * @property string $slack_token
 * @property string $webhook_url
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 *
 * Relations
 * @property UserNotificationSettings $notificationSettings
 */
class UserNotificationSettings extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'phone_number',
        'slack_channel',
        'slack_token',
        'webhook_url',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'user_id' => 'int',
    ];
}
