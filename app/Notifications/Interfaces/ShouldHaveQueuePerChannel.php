<?php

namespace App\Notifications\Interfaces;

interface ShouldHaveQueuePerChannel
{
    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array<string, string>
     */
    public function viaQueues(): array;
}
