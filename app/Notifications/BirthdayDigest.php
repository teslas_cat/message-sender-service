<?php

namespace App\Notifications;

use App\Enums\NotificationChannel;
use App\Models\User;
use App\Notifications\Interfaces\ShouldHaveQueuePerChannel;
use App\Notifications\Traits\HasQueuePerChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\VonageMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Slack\SlackMessage;
use Illuminate\Support\Facades\Log;
use NotificationChannels\Webhook\WebhookMessage;
use Throwable;

class BirthdayDigest extends Notification implements ShouldQueue, ShouldHaveQueuePerChannel
{
    use Queueable;
    use HasQueuePerChannel;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public int $tries = 3;

    /**
     * Create a new notification instance.
     */
    public function __construct(
        protected NotificationChannel $channel,
        protected string $body
    ) {
    }

    /**
     * Determine if the notification should be sent.
     */
    public function shouldSend(User $notifiable): bool
    {
        return $notifiable->canBeNotifiedViaChannel($this->channel);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(User $notifiable): array
    {
        return [$this->channel->transportClassName()];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(User $notifiable): MailMessage
    {
        return (new MailMessage())
            ->subject('Birthday Digest')
            ->greeting("Hello, $notifiable->name!")
            ->line($this->body);
    }

    /**
     * Get the Vonage / SMS representation of the notification.
     */
    public function toVonage(User $notifiable): VonageMessage
    {
        return (new VonageMessage())
            ->clientReference((string) $notifiable->id)
            ->content($this->body);
    }

    /**
     * Get the Slack representation of the notification.
     */
    public function toSlack(User $notifiable): SlackMessage
    {
        return (new SlackMessage())
            ->headerBlock('Birthday Digest')
            ->text($this->body);
    }

    /**
     * Get the Webhook representation of the notification.
     */
    public function toWebhook(User $notifiable): WebhookMessage
    {
        return WebhookMessage::create()
            ->data([
                'payload' => [
                    'message_type' => 'birthdayDigest',
                    'message_info' => $this->body,
                ]
            ]);
    }

    /**
     * @param Throwable $e
     * @return void
     */
    public function failed(Throwable $e): void
    {
        Log::error(
            'Failed to send a Birthday Digest notification via ' . $this->channel->name . ' channel.'
            . ' Last error: "' . $e->getMessage()
        );
    }
}
