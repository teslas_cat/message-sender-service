<?php

namespace App\Notifications\Traits;

use App\Enums\NotificationChannel;

trait HasQueuePerChannel
{
    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array<string, string>
     */
    public function viaQueues(): array
    {
        return [
            NotificationChannel::Email->transportClassName() => 'mail-queue',
            NotificationChannel::SMS->transportClassName() => 'sms-queue',
            NotificationChannel::Slack->transportClassName() => 'slack-queue',
            NotificationChannel::Webhook->transportClassName() => 'webhook-queue',
        ];
    }
}
