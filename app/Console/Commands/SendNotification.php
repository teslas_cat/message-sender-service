<?php

namespace App\Console\Commands;

use App\Enums\NotificationChannel;
use App\Enums\NotificationType;
use App\Models\User;
use Illuminate\Console\Command;
use Exception;
use InvalidArgumentException;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification
       {user_id : The ID of the user}
       {channel : Notification channel (mail,sms,slack,webhook)}
       {type : Notification type (birthdayDigest,invitation,reminder)}
       {body : Notification text}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notification of specified type to the user via supported channel with custom text.';

    /**
     * Execute the console command.
     */
    public function handle(): int
    {
        try {
            list($user, $channel, $type) = [$this->getUser(), $this->getChannel(), $this->getType()];
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return self::FAILURE;
        }

        if (!$user->canBeNotifiedViaChannel($channel)) {
            $this->error('This user can not be notified via specified channel.');
            return self::FAILURE;
        }

        try {
            $this->sendNotification($user, $type, $channel);
        } catch (Exception $e) {
            $this->error('Failed to send notification. Error: ' . $e->getMessage());
            return self::FAILURE;
        }

        return self::SUCCESS;
    }

    private function getUser(): ?User
    {
        return User::findOrFail($this->argument('user_id'));
    }

    private function getChannel(): ?NotificationChannel
    {
        $channel = NotificationChannel::tryFrom($this->argument('channel'));
        if (is_null($channel)) {
            throw new InvalidArgumentException(
                'Invalid channel provided. Valid options are:'
                . implode(',', NotificationChannel::values())
            );
        }

        return $channel;
    }

    private function getType(): ?NotificationType
    {
        $type = NotificationType::tryFrom($this->argument('type'));
        if (is_null($type)) {
            throw new InvalidArgumentException(
                'Invalid type provided. Valid options are:'
                . implode(',', NotificationType::values())
            );
        }
        return $type;
    }

    private function sendNotification(User $user, $type, $channel): void
    {
        $notificationInstance = $type->instanceClassName();
        $user->notify(new $notificationInstance($channel, $this->argument('body')));
    }
}
