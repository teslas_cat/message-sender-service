<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Support\Facades\Log;

class LogNotificationStatus implements ShouldQueue
{
    /**
     * Handle the event.
     */
    public function handle(NotificationSent $event): void
    {
        Log::info(
            class_basename($event->notification::class) . ' notification was successfully sent to User ID: '
            . $event->notifiable->id
            . ' via ' . $event->channel . '.'
        );
    }
}
