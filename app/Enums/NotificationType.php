<?php

namespace App\Enums;

use App\Notifications\BirthdayDigest;
use App\Notifications\Invitation;
use App\Notifications\Reminder;

enum NotificationType: string
{
    case BirthdayDigest = 'birthdayDigest';
    case Invitation = 'invitation';
    case Reminder = 'reminder';

    public function instanceClassName(): string
    {
        return NotificationType::getInstanceClassName($this);
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function getInstanceClassName(self $value): string
    {
        return match ($value) {
            NotificationType::BirthdayDigest => BirthdayDigest::class,
            NotificationType::Invitation => Invitation::class,
            NotificationType::Reminder => Reminder::class,
        };
    }
}
