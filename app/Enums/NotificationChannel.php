<?php

namespace App\Enums;

use Illuminate\Notifications\Channels\MailChannel;
use Illuminate\Notifications\Channels\VonageSmsChannel;
use Illuminate\Notifications\Slack\SlackChannel;
use NotificationChannels\Webhook\WebhookChannel;

enum NotificationChannel: string
{
    case Email = 'mail';
    case SMS = 'sms';
    case Slack = 'slack';
    case Webhook = 'webhook';

    public function transportClassName(): string
    {
        return NotificationChannel::getTransportClassName($this);
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function getTransportClassName(self $value): string
    {
        return match ($value) {
            NotificationChannel::Email => MailChannel::class,
            NotificationChannel::SMS => VonageSmsChannel::class,
            NotificationChannel::Slack => SlackChannel::class,
            NotificationChannel::Webhook => WebhookChannel::class,
        };
    }
}
