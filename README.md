# Queue-Based Message Sender Service

This Laravel-based application is designed to listen to a Redis queue, process incoming messages, and send them out to various channels based on defined templates.

## Features

- **Queue Listener:** Listens to messages in the queue and processes them.
- **Message Sending:** Sends messages to different channels (email, SMS, Slack, etc.) based on templates.
- **Logging:** Logs the status of outgoing messages.
- **Retry Mechanism:** Handles retries for failed message sending attempts.

## Tech Stack

- Laravel 10
- PHP 8.2
- Docker-compose

## Installation and Setup

1. **Clone the Repository:**

   ```bash
   git clone https://gitlab.com/teslas_cat/message-sender-service.git
   cd queue-messages-app
   ```

2. **Set Up Environment Variables:**

   Create a `.env` file by copying `.env.example` and configure your database and other necessary settings:

    ```bash
   cp .env.example .env
   ```

3. **Install Dependencies:**

   ```bash
   docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
   ```

4. **Start Docker containers in the background:**

   ```bash
   ./vendor/bin/sail up -d
   ```
   Also you can configure a shell alias that allows you to execute Sail's commands more easily:

    ```bash
    alias sail='[ -f sail ] && sh sail || sh vendor/bin/sail'
    ```

5. **Generate Application Key:**

   ```bash
   sail artisan key:generate
   ```

6**Run Migrations:**

   ```bash
   sail artisan migrate
   ```

## Usage

1. **Queue Listener:**

    - The application will automatically listen to the Redis queue.
    - Messages in the queue will be processed, and outgoing messages will be sent based on the content.

2. **Testing:**

    - Use the provided console command to simulate sending messages for testing different channels, types, and bodies.

   ```bash
   php artisan send:message {channel} {type} {body}
   ```

   Replace `{channel}`, `{type}`, and `{body}` with the desired parameters.
